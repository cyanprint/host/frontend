FROM kirinnee/frontend-gitlab-runner:latest
COPY . .
RUN yarn --prefer-offline
RUN yarn build
