import Vue from 'vue';
import './index.scss';
import 'highlight.js/styles/github.css';
import {router} from "./router";
import {images} from './images';
import AuthPlugin from "./plugins/AuthPlugin";
import App from "./App.vue";
import SdkPlugin from "./plugins/SdkPlugin";

Vue.config.productionTip = false;
Vue.use(AuthPlugin);
Vue.use(SdkPlugin);

function CopyToClipBoard(s: string): void {
	const ta: HTMLTextAreaElement = document.getElementById("clipboard-sim")! as HTMLTextAreaElement;
	ta.value = s;
	ta.select();
	document.execCommand("copy");
}

new Vue({
	render: h => h(App),
	router,
}).$mount('#app');

export {
	images,
	CopyToClipBoard
}
