import logo from "./assets/logo.png"
import logoPlain from "./assets/logo_no_text.png";
import gitlab from "./assets/gitlab.png";
import dl from "./assets/dl.png";
import star from "./assets/star.png";
import git from "./assets/git.png";
import author from './assets/author.png';
import {Rimage, Rimager} from "@kirinnee/rimage";
import {SortType} from "@kirinnee/core";
import {core} from "./init";

core.AssertExtend();

function obtainImage(i: string): string {
	return require(`./assets/tutorial/create/${i}.jpg`);
}

let images: any = {
	logo,
	logoPlain,
	gitlab,
	dl,
	star,
	git,
	author,
	home: {
		customizable: require('./assets/home/customizable.png'),
		focus: require('./assets/home/focus.png'),
		production: require('./assets/home/production_ready.png'),
		simplicity: require('./assets/home/simplicity.png'),
	},
	tutorial: {
		create: {
			step1: obtainImage("step1"),
			step2: obtainImage("step2"),
			step3: obtainImage("step3"),
			step4: obtainImage("step4"),
			step51: obtainImage("step5-1"),
			step52: obtainImage("step5-2"),
			step6: obtainImage("step6"),
			step7: obtainImage("step7"),
			step8: obtainImage("step8"),
			step9: obtainImage("step9"),
			step10: obtainImage("step10"),
		},
		use: {
			ss1: require('./assets/tutorial/use/choose_main.jpg'),
			ss2: require('./assets/tutorial/use/choose_vue_template.jpg'),
			ss3: require('./assets/tutorial/use/config.jpg'),
		}
	}
};

declare var PRODUCTION: boolean;
let imageDeployConfig: any = require("../../../config/image.deploy.config.json")[0];

let config: Rimage = {
	key: imageDeployConfig.key,
	width: imageDeployConfig.width,
	sizes: imageDeployConfig.sizes
};

let rimager: Rimager = new Rimager(core, config, new SortType(), !PRODUCTION);
rimager.ExtendPrimitives();

images = rimager.RegisterImages(images);

export {
	images,
	rimager
}
