
!!!!

Enter the remote repository link. **This repository has to be a `https://` remote link.** 

Without this,
you cannot publish any templates. [GitLab](https://gitlab.com) is highly recommended as CyanPrint has
in-built support for GitLab's CI for CyanPrint, which can directly publish your template on code push!
