# Auto CI with GitLab

If you have generated the `.gitlab-ci.yml` together previously during scaffolding, and host your 
template repository on [GitLab](https://gitlab.com), you can easily enable CI with GitLab.


1. Add `CYANPRINT_TOKEN` as a variable to your GitLab CI settings:
    Project > Settings > CI/CD > VARIABLES
       
     Do note that you **should** protect your variable and only allow trusted
     people to have access to this value.
     
2. If you do not have `.gitlab-ci.yml` scaffolded, add the file with the following content:
    ```
    image: kirinnee/frontend-gitlab-runner:latest
    stages:
      - deploy
    
    Deploy:
      only:
        - master
      stage: deploy
      script:
        cyan push $CYANPRINT_TOKEN

    ``` 

3. Now, each time you merge or push to master, the repository will be automatically deployed to
    CyanPrint server!
    
Congratulations, you have enabled auto CI/CD with GitLab!
