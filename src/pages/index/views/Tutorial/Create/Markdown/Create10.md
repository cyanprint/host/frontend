!!!!

Choose whether you want to have CI/CD integrated with GitLab. If you enable this and your repository is 
in [GitLab](https://gitlab.com), your repository will automatically update the CyanPrint host each time
your host is pushed. (Automated deployment of templates). This requires you to set you secret variable
`CYANPRINT_TOKEN` to your Personal Access Token. You can more about it at [publishing](/tutorial/create/publish)
and [auto CI with GitLab](/tutorial/create/gitlab).

