


 # Create your first CyanPrint
 
 Scaffold a CyanPrint Template project from the pre-installed CyanPrint template (#META)
 ```bash
$ cyan c "My First CyanPrint"
```

!!!!

Select the `Inbuilt Templates` option. Here we are looking through the series of default templates
> You can move between options with your keyboard `up arrow` and `down arrow` key 
<br> To select the option, press `Enter` on your keyboard when your selector is on the option you want

    

    

    




    
    
 

