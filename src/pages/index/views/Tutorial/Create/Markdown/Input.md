# Requiring User Input

CyanPrint allows template writers to easily ask for inputs from user. Underneath, it uses 
[Inquirer](https://github.com/SBoudrias/Inquirer.js/) and [Chalk](https://github.com/chalk/chalk)
to accomplish feats, and for template writers familiar with Javascript and these tools, you can use them for more 
control over how the questions is asked and value is stored.

CyanPrint providers a higher level interface to allow users to easily inquire inputs from the user - the 
`autoInquirer` object.

Below, we will show a demo on how to inquire user input for `name.organization`, `name.person` and `email` from
the previous step.

!!!!

In your `cyan.config.js`, change it to the content below:

```javascript
module.exports = async function(folderName, chalk, inquirer, autoInquirer, autoMapper, execute) {
    let inputs =  {
         name:{
             person: "bob", //put the default value for name instead of bob
             organization: "illuminati" //put the default value for organization name instead of illuminati
         },
         email: "bob@gmail.com" //put the default value for email instead of bob@gmail.com
    };
    
    inputs = await autoInquirer.InquireInput(inputs); 
     
    return {
        globs: {root:"./Template",pattern: "**/*.*", ignore: ""},
        variable: inputs
    };
};
```

This requests the user to input a value for each field (name.organization), 
and if no input was detected it will default the be value that was first set (illuminati).

You can try it out with the `try` command:

```bash
$ cyan try . sample
```

!!!!

Congratulations! Now lets move on to how to publish your template to the host repository!

!!!!



