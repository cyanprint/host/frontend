# Writing the template itself

In your newly created folder `My First CyanPrint`, you should have the folder structure:
```
 My First CyanPrint (root)
 |- cyan.config.js
 |- cyan.json
 |- /Template
    |- .gitignore
```

You may or may not have a `.gitlab-ci.yml`, `README.MD`, `CONTRIBUTING.MD` and `LICENSE.MD` at the top level depending on your 
choice when generating the project.

> **From here on, all commands are ran in the terminal which path is at root (My First 
CyanPrint) folder**

!!!!

Let's recall our project aim from the [start](/tutorial/create/start):

> In this section, we will aim to create a simple template that ask the user for its name, 
organization and email, replacing the file name with the organization name  change the 
file content to:

> ```
> Hello `name`
> Your email is `email`
> ```

!!!!

Now we start!

1. Create a new text file, called `var~name.organization~.txt`
    ```bash
    $ touch Template/var~name.organization~.txt
    ```

2. Next open up `var~name.organization~.txt` with any text editor and write in the following lines:
    ```
    Hello var~name.person~!
    Your email is: var~email~
    ```

3. Now change your `cyanprint.config.js` to below:
    ```javascript
    module.exports = async function(folderName, chalk, inquirer, autoInquirer, autoMapper, execute) {       
       return {
            globs: {root:"./Template",pattern: "**/*.*", ignore: ""},
            variable: {
                name:{
                    person: "bob", //put the default value for name instead of bob
                    organization: "illuminati" //put the default value for organization name instead of none
                },
                email: "bob@gmail.com" //put the default value for email instead of bob@gmail.com
            }
        };
    };
    ```
    
4. Generate code from your command!
    ```bash
    $ cyan try . awesome-project
    ```
    
!!!!

A folder called `awesome-project` should be generated (from the current template) in the folder.

The folder `awesome-project` would be the result of your template! 

!!!!

Your `awesome-project` folder should have this structure:
```
|- /awesome-project
   |- .gitignore
   |- illuminati.txt
```

and the contents of `illuminati.txt` should contain:
```
Hello bob!
Your email is: bob@gmail.com
```

!!!!

As you can see, variables wrapped in `var~` and `~` are replaced:

`name.organization` -> `illuminta`

`name.person` -> `bob`  

`email` -> `bob@gmail.com`

This corresponds to the JSON provided in the `variable` field in the return type of the function.


!!!!

Congratulations! You have successfully created your first CyanPrint template!

!!!!

Next, you would learn how to inquire user input and replace the templates!

!!!!
