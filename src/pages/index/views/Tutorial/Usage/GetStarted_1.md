# Prerequisite

- [Node.js](https://nodejs.org/en/) 
- [NPM](https://www.npmjs.com/) or [Yarn](https://yarnpkg.com/en/) (Only need either)
- [Git](https://git-scm.com/)

> Please visit each respective site for installation instructions 

To check whether you have the dependency installed, use the commands below:
    
<div class='b-flex'>

**Git**
```bash
$ git --version
```

</div>

    
<div class='b-flex'>

**Node**
```bash
$ node -v
```

</div>

<div class='b-flex'>

**NPM**
```bash
$ npm -v
```

</div>

<div class='b-flex'>

**Yarn**
```bash
$ yarn -v
```

</div>

If they respond with a version, they have been installed.

# Installation

Installation can be done via [NPM](https://www.npmjs.com/) or [Yarn](https://yarnpkg.com/en/).

**NPM**
```bash
$ npm i -g cyanprint
```

**Yarn**
```bash
$ yarn global add cyanprint
```

!!!!

Check that installation is complete:

```bash
$ cyan -v
```

# Installing templates

CyanPrint is a scaffolding engine, you have to first install templates from our online repositories
of templates, thereafter use the installed template to scaffold projects.

> You can look for templates using the search bar at the top of this page.

!!!!

For this tutorial, we explain how to install the [Vue 2 template](/template/vue2)  

The `Vue 2 Template` has the key of `vue2`, this can be found on the template's page itself, and
that will be used for the installation:

<div class='b-flex'>

```bash
$ cyan install vue2
```

or

```bash
$ cyan i vue2
```
</div>

> ProTip: `i` is a shorthand for `install`

!!!!

The `vue2` template would have been installed into your machine.

# Using templates

After installing any templates, all you have to do to use the templates is to run 
the `create` command.

> The command has to be ran in the correct folder

<div class='b-flex'>

```bash
$ cyan create app
```

or 
```bash
$ cyan c app
```
</div>

> ProTip: `c` is a shorthand for `create`

The command creates the template at the `app` folder, following the command 
syntax of `cyan create <folder-name>`

!!!!

The CyanPrint CLI would ask you to first choose the group you want (by default, everything
is installed to the `Inbuilt Teplate` group). 

> You can use your keyboards' 'up-arrow' and 'down-arrow' key to navigate

> Press 'Enter' to choose your select what you want 









