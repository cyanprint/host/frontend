# Update a Template

To update a template, you need the `group key` and `template key`

!!!!

Run the following command: 

<div class='b-flex'>

```bash
$ cyan upgrade <group_key> <template_key>
```

or

```bash
$ cyan u <group_key> <template_key>
```

</div>

The template in the specific folder and key will be updated to the
latest

# Update a Local Group
Updating the local group will simply update every single template
within the group to the latest version.

This does not check the online repository for the latest group
definition, and does not need the group to be online.

All you need is the `group key`

!!!!

Run the following command:

<div class='b-flex'>

```bash
$ cyan upgrade <group_key>
```

or

```bash
$ cyan u <group_key> 
```

</div>


Every template within the specified group locally will be updated
to the latest template

# Update a Remote Group
Updating from the remote will first download the newest group
definition from the online repository. After which, it will update
all the template to the latest template, remove templates that no
longer belongs to the group and add new templates to the group base
on the newest definition

This checks the online repository for the latest group
definition, and requires the group to be online.

All you need is the `group key`

!!!!

Run the following command:

<div class='b-flex'>

```bash
$ cyan group update <group_key>
```

or

```bash
$ cyan g u <group_key> 
```

</div>

After the command completes, every template in the group will be updated to 
the remote group's definition


# Update all local templates

This will update all templates within all group to the latest version, if
possible.

!!!!

Run the following command:

<div class='b-flex'>

```bash
$ cyan update
```

or

```bash
$ cyan u
```

</div>

After the command completes, every template in each group will be updated to the latest

!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
!!!!
