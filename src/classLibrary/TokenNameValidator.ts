import {Core} from "@kirinnee/core";

class TokenNameValidator {
	
	constructor(core: Core) {
		core.AssertExtend();
	}
	
	Valid(input: string): string {
		if (!this.AtLeastOneCharacter(input))
			return 'This field cannot be empty';
		if (!this.ContainOnlyAlphanumericUnderscoreDash(input))
			return 'Can only contain alphanumeric, underscore and dashes';
		if (!this.StartsWithAlphabet(input))
			return 'Can only start with alphabets';
		return '';
	}
	
	StartsWithAlphabet(test: string): boolean {
		return test.split('')[0].Match(/[a-zA-Z]/).length === 1;
	}
	
	ContainOnlyAlphanumericUnderscoreDash(test: string): boolean {
		return /^[0-9a-z_\-]*$/i.test(test);
	}
	
	AtLeastOneCharacter(test: string): boolean {
		return test.trim().length > 0;
		
	}
	
	
}

export {TokenNameValidator}
