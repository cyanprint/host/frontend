interface TemplateFlyWeightLiteral {
	display_name: string;
	unique_key: string;
	author: string;
	repository: string;
	downloads: number;
	stars: number;
	readme: string;
}

class TemplateFlyWeight {
	literal: TemplateFlyWeightLiteral;
	
	constructor(literal: TemplateFlyWeightLiteral) {
		this.literal = literal;
	}
}

export {TemplateFlyWeight, TemplateFlyWeightLiteral}
