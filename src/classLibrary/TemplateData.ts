interface TemplateDataLiteral {
	display_name: string,
	unique_key: string,
	author: string,
	repository: string,
	downloads: number,
	stars: number,
	readme: string,
	group: string[]
}

class TemplateData {
	literal: TemplateDataLiteral;
	
	constructor(literal: TemplateDataLiteral) {
		this.literal = literal;
	}
}

export {TemplateData, TemplateDataLiteral}
