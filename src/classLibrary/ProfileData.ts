interface ProfileData {
	aud: string
	email: string
	email_verified: boolean
	exp: number
	iat: number
	iss: string
	name: string
	nickname: string
	nonce: string
	picture: string
	sub: string
	updated_at: Date
}

class ProfileDataObject {
	constructor(literal: ProfileData) {
		this.literal = literal;
	}
	
	literal: ProfileData
}

export {ProfileData, ProfileDataObject}
