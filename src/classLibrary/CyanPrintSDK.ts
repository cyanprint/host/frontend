import {InitialToken, Token} from "./Token";
import {AuthService} from "./AuthService";
import EventEmitter from 'events';
import {ApiError} from "./ApiError";
import {TemplateFlyWeight, TemplateFlyWeightLiteral} from "./TemplateFlyWeight";
import {$$} from "../pages/index/init";
import {GroupData, GroupDataLiteral} from "./GroupData";
import {TemplateData, TemplateDataLiteral} from "./TemplateData";

class CyanPrintSDK extends EventEmitter {
	public static error = {e: "e"};
	public readonly host: string;
	private readonly auth: AuthService;
	public readonly eventKey: string = "apiError";
	
	private Endpoint(path: string): string {
		return `${this.host}/${path}`;
	}
	
	private Reject(name: string, description: string, code: number): Promise<any> {
		const error: ApiError = {
			message: {
				message: description
			},
			name,
			type: code
		};
		this.emit(this.eventKey, error);
		return Promise.reject()
	}
	
	constructor(host: string, auth: AuthService) {
		super();
		this.host = host;
		this.auth = auth;
	}
	
	async getAllToken(): Promise<Token[]> {
		return await this.ping<Token[]>(this.Endpoint('token'));
	}
	
	async CreateToken(input: string, days: number): Promise<InitialToken> {
		if (input.IsEmpty()) return this.Reject("Validation", "Please enter key-name", 400);
		if (days < 0) return this.Reject("Validation", "Please enter positive token lifespan", 400);
		const body: any = {
			"expire_in": days,
			"key": input
		};
		return this.ping<InitialToken>(this.Endpoint('token'), 'POST', body);
	}
	
	async DeleteToken(tokenId: number): Promise<void> {
		return this.ping(this.Endpoint(`token/${tokenId}`), 'DELETE');
	}
	
	async SearchTemplates(filter: string): Promise<TemplateFlyWeight[]> {
		await $$(0);
		const templates: TemplateFlyWeightLiteral[] = await this.ping(this.Endpoint(`template?filter=${filter}`));
		return templates.Map(e => new TemplateFlyWeight(e));
	}
	
	async SearchGroups(filter: string): Promise<GroupData[]> {
		await $$(0);
		const groups: GroupDataLiteral[] = await this.ping(this.Endpoint(`group?filter=${filter}`));
		return groups.Map(e => new GroupData(e));
	}
	
	async IsGroupStarred(key: string): Promise<boolean> {
		const {like}: { like: boolean } = await this.ping(this.Endpoint(`group/${key}/like`));
		return like;
	}
	
	async ToggleGroupStar(key: string, star: boolean): Promise<boolean> {
		const {like}: { like: boolean } = await this.ping(this.Endpoint(`group/${key}/like`), 'PUT',
			{like: star});
		return like;
	}
	
	async GetGroup(key: string): Promise<GroupData> {
		const group: GroupDataLiteral = await this.ping(this.Endpoint(`group/${key}`));
		return new GroupData(group);
	}
	
	async IsTemplateStarred(key: string): Promise<boolean> {
		const {like}: { like: boolean } = await this.ping(this.Endpoint(`template/${key}/like`));
		return like;
	}
	
	async ToggleTemplateStar(key: string, star: boolean): Promise<boolean> {
		const {like}: { like: boolean } = await this.ping(this.Endpoint(`template/${key}/like`), 'PUT',
			{like: star});
		return like;
	}
	
	async GetTemplate(key: string): Promise<TemplateData> {
		const template: TemplateDataLiteral = await this.ping(this.Endpoint(`template/${key}`));
		return new TemplateData(template);
	}
	
	async GetUserTemplate(): Promise<TemplateFlyWeight[]> {
		const templates: TemplateFlyWeightLiteral[] = await this.ping(this.Endpoint(`user/template`));
		return templates.Map(e => new TemplateFlyWeight(e));
	}
	
	async GetUserStarredTemplate(): Promise<TemplateFlyWeight[]> {
		const templates: TemplateFlyWeightLiteral[] = await this.ping(this.Endpoint(`user/template/like`));
		return templates.Map(e => new TemplateFlyWeight(e));
	}
	
	async GetUserGroup(): Promise<GroupData[]> {
		const group: GroupDataLiteral[] = await this.ping(this.Endpoint(`user/group`));
		return group.Map(e => new GroupData(e));
	}
	
	async GetUserStarredGroup(): Promise<GroupData[]> {
		const group: GroupDataLiteral[] = await this.ping(this.Endpoint(`user/group/like`));
		return group.Map(e => new GroupData(e));
	}
	
	
	async ping<T>(endpoint: RequestInfo, method: string = "GET", body: any = null, headers: HeadersInit = {
		accept: "application/json",
		'Content-Type': 'application/json'
	}): Promise<T> {
		try {
			return await this.fetch(endpoint, {
				body: body == null ? null : JSON.stringify(body),
				method,
				headers
			});
		} catch (e) {
			this.emit(this.eventKey, e);
			return CyanPrintSDK.error as any as T;
		}
	}
	
	async fetch<T>(input: RequestInfo, init?: RequestInit): Promise<T> {
		if (this.auth.isAuthenticated()) {
			const authorization: string = `Bearer ${this.auth.idToken}`;
			if (init == null) init = {};
			if (init.headers == null) init.headers = {};
			(init.headers as any)['authorization'] = authorization;
		}
		const response: Response = await fetch(input, init);
		if (response.status === 204) {
			return {} as T;
		}
		const body = await response.json();
		if (!response.ok) return Promise.reject({name: "Error", message: body, type: response.status});
		return body;
	}
	
}

export {CyanPrintSDK}
