interface Token {
	id: number
	expiry: Date,
	key: string,
}

interface InitialToken extends Token {
	secret: string;
}

export {Token, InitialToken}
